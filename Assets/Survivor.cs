﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Survivor
{
    public string SurvivorId { get; set; }
    public string HealthStatus { get; set; }
    public string WorkTask { get; set; }
    public Dictionary<Skills, float> SkillPercentages {get;set;}

}
