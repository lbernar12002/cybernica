﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivorManager : MonoBehaviour
{

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public enum Skills
{
    Scavenging,
    Scrapping,
    Demolition,
    Hydroponics,
    AnimalHusbandry,
    Research,
    Construction,
    ForensicDataMining,
    Dismantalling,
    MechanicalRepair,
    ComputerAndElectronicRepair,
    Medical,
    Smelting,
    MetalWorking,
    ElectronicAssembly,
    DroidEngineering,
    ComputerSpecialist,
    CommunicationsSpecialist
}

public enum HealthStatuses
{
    Healthy,
    Sick,
    BedRidden,
    Injured,
    BadlyInjured,
    Wounded,
    BadlyWounded
}

public enum WorkTasks
{
    Nothing,
    Scavenge,
    Treat,
    Cook
}
